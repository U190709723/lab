import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99." );
		System.out.print("Can you guess it: ");
		
		
		
		int guess;
		int attempt=0;
		do{
			attempt++;
			System.out.print("Type -1 to quit or guess another ("+number+"):");
			guess = reader.nextInt();

			if(guess == -1)
				System.out.println("Sorry, the number was "+number);
			else if(guess>number){
				System.out.println("Mine ise less than your guess:");
				System.out.println("----------------------------------");
			}
				
			else if(guess<number){
				System.out.println("Mine ise greater than your guess:");
				System.out.println("----------------------------------");
			}	
			else if(guess == number)
				System.out.println("Congrulations! You won after "+attempt+" attempts");
			
			
			

		}while(guess != -1 && guess != number);
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
