public class FindLetterGrade{
public static void main(String[] args){
int score=Integer.parseInt(args[0]);

if (100>=score && score>=90)
	System.out.println("A");

else if (90>score && score>=80)
	System.out.println("B");

else if (80>score && score>=70)
	System.out.println("C");

else if (70>score && score>=60)
	System.out.println("D");

else if (60>score)
	System.out.println("F");

else
	System.out.println("Try a valid score,please");

}
}
